#include "lCD.h"	
#include	"STC8G_H_Delay.h"

#define lcd_buff_length (3)
#define COM1 P14
#define COM2 P13
#define COM3 P12
#define COM4 P11
#define SEG1 P15
#define SEG2 P00
#define SEG3 P01
#define SEG4 P02
#define SEG5 P03
#define SEG6 P10
/*************	本地常量声明	**************/
u8 code t_display[]={						//标准字库
//	 0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
	0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F,0x77,0x7C,0x39,0x5E,0x79,0x71,
//black	 -   
	0x00,0x40
};
/*
MCU PIN        P17   P16   P15  P14 P13  P12  P11  P10  P27  P26  P25  P24   P23  P22  P21  P20
LCD PIN         1     2     3    4   5    6    7    8    9    10   11   12    13   14   15   16
LCD PIN name  SEG1 SEG10 SEG9 SEG8 SEG7 SEG6 SEG5 SEG4 SEG3 SEG2 SEG1 SEG0  COM3 COM2 COM1 COM0
               --    1D    2:   2D   2.   3D   4:   4D   4.   5D   5.   6D   COM3
			   1E    1C    2E   2C   3E   3C   4E   4C   5E   5C   6E   6C        COM2
			   1G    1B    2G   2B   3G   3B   4G   4B   5G   5B   6G   6B             COM1
			   1F    1A    2F   2A   3F   3A   4F   4A   5F   5A   6F   6A                  COM0

			B7	B6	B5	B4	B3	B2	B1	B0

buff[0]:	--	1D	2:	2D	2.	3D	4:	4D
buff[1]:	1E	1C	2E	2C	3E	3C	4E	4C
buff[2]:	1G	1B	2G	2B	3G	3B	4G	4B
buff[3]:	1F	1A	2F	2A	3F	3A	4F	4A
buff[4]:	4.	5D	5.	6D	--	--	--	--
buff[5]:	5E	5C	6E	6C	--	--	--	--
buff[6]:	5G	5B	6G	6B	--	--	--	--
buff[7]:	5F	5A	6F	6A	--	--	--	--

MCU PIN        P15   P14  P13   P12    P11     P00  P01  P02  P03  P10
LCD PIN         1     2    3     4      5       6    7    8    9    10
LCD PIN name   SEG1  COM1 COM2  COM3   COM4   SEG2  SEG3 SEG4 SEG5 SEG6
*/
/*************	本地变量声明	**************/
u8	LCD_buff[lcd_buff_length];
u8	scan_index=0;
u8 code t_display_code1[][4]=//com1
	{
		{0x18,0x10,0x18,0x08},//0
		{0x08,0x00,0x08,0x00},//1
		{0x10,0x18,0x08,0x08},//2
		{0x18,0x08,0x08,0x08},//3
		{0x08,0x08,0x18,0x00},//4
		{0x18,0x08,0x10,0x08},//5
		{0x18,0x18,0x10,0x08},//6
		{0x08,0x00,0x08,0x08},//7
		{0x18,0x18,0x18,0x08},//8
		{0x08,0x08,0x18,0x08},//9
		{0x10,0x18,0x10,0x08},//E
		{0x00,0x18,0x00,0x00},//r
		{0x08,0x18,0x18,0x08},//A
		{0x00,0x18,0x18,0x08},//P
		{0x10,0x10,0x10,0x00},//L
		{0x18,0x18,0x10,0x00},//b
	};

u8 code t_display_code2[][4]=//com2
	{
		{0x06,0x04,0x06,0x02},//0
		{0x02,0x00,0x02,0x00},//1
		{0x04,0x06,0x02,0x02},//2
		{0x06,0x02,0x02,0x02},//3
		{0x02,0x02,0x06,0x00},//4
		{0x06,0x02,0x04,0x02},//5
		{0x06,0x06,0x04,0x02},//6
		{0x02,0x00,0x02,0x02},//7
		{0x06,0x06,0x06,0x02},//8
		{0x02,0x02,0x06,0x02},//9
		{0x04,0x06,0x04,0x02},//E
		{0x00,0x06,0x00,0x00},//r
		{0x02,0x06,0x06,0x02},//A
		{0x00,0x06,0x06,0x02},//P
		{0x04,0x04,0x04,0x00},//L
		{0x06,0x06,0x04,0x00},//b
	};
u8 code t_display_code3[][4]=//com3
	{
		{0x21,0x01,0x21,0x20},//0
		{0x20,0x00,0x20,0x00},//1
		{0x01,0x21,0x20,0x20},//2
		{0x21,0x20,0x20,0x20},//3
		{0x20,0x20,0x21,0x00},//4
		{0x21,0x20,0x01,0x20},//5
		{0x21,0x21,0x01,0x20},//6
		{0x20,0x00,0x20,0x20},//7
		{0x21,0x21,0x21,0x20},//8
		{0x20,0x20,0x21,0x20},//9
		{0x01,0x21,0x01,0x20},//E
		{0x00,0x21,0x00,0x00},//r
		{0x20,0x21,0x21,0x20},//A
		{0x00,0x21,0x21,0x20},//P
		{0x01,0x01,0x01,0x00},//L
		{0x21,0x21,0x01,0x00},//b
	};


/****************** 初始化LCD显存函数 ***************************/

void	Init_LCD_Buffer(void)
{
	u8	i;

	for(i=0; i<lcd_buff_length; i++)	
	{
		LCD_buff[i] = 0xff;
	}
	P0_MODE_OUT_PP(0x0f);
	P1_MODE_OUT_PP(0x21);
}

/****************** LCD段码扫描函数 ***************************/

u8	code T_COM[4]={0x10,0x08,0x04,0x02};

void	LCD_scan(void)	
{
	u8 show_data;
	P1_MODE_IN_HIZ(0x1E);		//全部COM输出高阻, COM为中点电压
	
	switch(scan_index)
	{
		case 0:
			P1_MODE_OUT_PP(T_COM[0]);//某个COM设置为推挽输出
			COM1=1;
		
			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][0]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][0]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][0]);
			}
			show_data=~show_data;
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;
		
		break;
		case 1:
			P1_MODE_OUT_PP(T_COM[0]);
			COM1=0;
		
			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][0]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][0]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][0]);
			}
			
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;

		break;
			
		case 2:
			P1_MODE_OUT_PP(T_COM[1]);
			COM2=1;
		
			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][1]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][1]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][1]);
			}
			show_data=~show_data;
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;

		break;
		case 3:
			P1_MODE_OUT_PP(T_COM[1]);
			COM2=0;
		
			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][1]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][1]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][1]);
			}
			
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;

		break;
		case 4:
			P1_MODE_OUT_PP(T_COM[2]);
			COM3=1;

			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][2]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][2]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][2]);
			}
			show_data=~show_data;
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;
		break;
		case 5:
			P1_MODE_OUT_PP(T_COM[2]);
			COM3=0;

			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][2]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][2]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][2]);
			}
			
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;
		break;
		case 6:
			P1_MODE_OUT_PP(T_COM[3]);
			COM4=1;
		
			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][3]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][3]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][3]);
			}
			show_data=~show_data;
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;
		
		break;
		case 7:
			P1_MODE_OUT_PP(T_COM[3]);
			COM4=0;
		
			show_data=0;
			if(LCD_buff[0]!=0xff)
			{
				show_data =show_data|(t_display_code1[LCD_buff[0]][3]);
			}
			if(LCD_buff[1]!=0xff)
			{
				show_data =show_data|(t_display_code2[LCD_buff[1]][3]);
			}
			if(LCD_buff[2]!=0xff)
			{
				show_data =show_data|(t_display_code3[LCD_buff[2]][3]);
			}
			
			SEG1=(show_data&0x20)>>5;
			SEG2=(show_data&0x10)>>4;
			SEG3=(show_data&0x08)>>3;
			SEG4=(show_data&0x04)>>2;
			SEG5=(show_data&0x02)>>1;
			SEG6=(show_data&0x01)>>0;
		break;
	}


	
	scan_index++;
	if(scan_index >7)
	{
		scan_index = 0;
	}

}




void	LCD_load(u8 n,u8 dat)		//n为第几个数字，为1~3，dat为要显示的数字	10us@22.1184MHZ
{
	LCD_buff[n]=dat;

}
u8 lcd_test_flag=0;
void	LCD_test(void)
{
	LCD_load(0,lcd_test_flag);
	LCD_load(1,lcd_test_flag);
	LCD_load(2,lcd_test_flag);
	lcd_test_flag++;
	if(lcd_test_flag==16)
	{
		lcd_test_flag=0;
	}
}	