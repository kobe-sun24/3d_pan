#include	"device_status.h"
#include	"config.h"
#include    "motor.h"
#include    "heat.h"
#include    "nRF24l01p.h"
#include    "lcd.h"
#include    "adc.h"	
#include    "led.h"	
user_dev_status_t user_dev_status;//=
//{
//	.Now_Status=POWER_INIT,
//	.Now_Temperature=190,
//	.Now_Speed=SLEEP_MAX,
//	.Now_Motor_Status=0,
//};

user_dev_status_t * _g_pDEVMgr = &user_dev_status;

user_dev_status_t *  dooya_get_dev_info(void)
{
	return _g_pDEVMgr;
}
void dev_status_init(void)
{
	uint8 i;
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	dev_tmp->Now_Status=POWER_INIT;
	dev_tmp->Target_Temperature=512;
	dev_tmp->Now_Speed=SPEED_MAX;
	dev_tmp->Now_Motor_Status=0;
	dev_tmp->Now_Adc_index=0;
	dev_tmp->Show_Temperature=0;
	i=0;
	for (i;i<ADC_MAX;i++)
	{
		set_dev_ADC_val(i,0);
	}
	dev_tmp->change_number_count=0;
	dev_tmp->Temperature_change_flag=0;
	dev_tmp->sleep_number_count=0;

	
	dev_tmp->Now_Temperature=0;
    dev_tmp->err=0.0;
    dev_tmp->err_last=0.0;
    dev_tmp->voltage=0;//定义电压值（控制执行器的变量）
    dev_tmp->integral=0.0;//定义积分值
    dev_tmp->Kp=0.2;
    dev_tmp->Ki=0.0005;//0.001;
    dev_tmp->Kd=0;
	
}


uint8 get_dev_Status(void  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Status;
}

int16 get_dev_Temperature(void  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Temperature;
}

int16 get_dev_ShowTemperature(void  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Show_Temperature;
}

int16 get_dev_NewTemperature(void  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Temperature;
}

uint8 get_dev_Speed(void)
{
	user_dev_status_t *dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Speed;
}

uint8 get_dev_Motor_Status(void  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Motor_Status;
}

uint16 get_dev_ADC_val(uint16       data_index)
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Adc_val[data_index];
}

uint16 get_dev_adc_index(void  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	return dev_tmp->Now_Adc_index;
}


/***********************************************************************/
void set_dev_Status(uint8 data_tmp  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	if(dev_tmp->Now_Status!=data_tmp)
	{

	}
	dev_tmp->Now_Status=data_tmp;
}

void set_dev_Temperature(int16 data_tmp  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	if(dev_tmp->Now_Temperature!=data_tmp)
	{
	}
	dev_tmp->Now_Temperature=data_tmp;
}

void set_dev_ShowTemperature(int16 data_tmp  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	dev_tmp->Show_Temperature=data_tmp;
}

void set_dev_Speed(uint8 data_tmp  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	if((dev_tmp->Now_Speed!=data_tmp)&&(dev_tmp->Now_Motor_Status))
	{
		//run
		motor_run_leavel(data_tmp);
	}
	dev_tmp->Now_Speed=data_tmp;
}

void set_dev_Motor_Status(uint8 data_tmp  )
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	if(dev_tmp->Now_Motor_Status!=data_tmp)
	{

	}
	dev_tmp->Now_Motor_Status=data_tmp;
}

void set_dev_ADC_val(uint16       data_index,uint16 data_tmp)
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	dev_tmp->Now_Adc_val[data_index]=data_tmp;
}

void set_dev_ADC_index(uint16       data_index)
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	dev_tmp->Now_Adc_index=data_index;
}

/*******************************************************************/
void motor_run_leavel(uint8 leavel)
{

	uint8 tmp;
	switch(leavel)
	{
		case 0:
			tmp=10;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);
		break;
		case 1:
			tmp=13;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 2:
			tmp=16;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 3:
			tmp=18;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 4:
			tmp=20;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 5:
			tmp=22;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 6:
			tmp=24;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 7:
			tmp=26;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 8:
			tmp=28;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
		case 9:
			tmp=30;
			motor_set_status(MOTOR_FORWARD,0xff,tmp, MOTOR_ALL_CYCLE-tmp);

		break;
	}
}


/****************************************************************************/
void PID_realize(void)
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	uint16 i,j;
	uint16 adc_tmp=0;
	float pid_add_tmp;
	if(dev_tmp->Now_Status==HEATING||dev_tmp->Now_Status==HEATED)
	{
	    //dev_tmp->Target_Temperature=Temperature_tmp;//目标温度
		i=0;
		j=0;
		for (i;i<ADC_MAX;i++)
		{
			if(get_dev_ADC_val(i)!=0)
			{
				adc_tmp=adc_tmp+get_dev_ADC_val(i);
				j++;
			}
			
		}
		adc_tmp=adc_tmp/j;
		printf("adc_tmpV is %d\r\n",1023-adc_tmp);
		
		dev_tmp->Now_Temperature=1023-adc_tmp;//当前温度，现在需要表格


	  dev_tmp->err=dev_tmp->Target_Temperature-dev_tmp->Now_Temperature;//误差
		
		printf("err is %f\r\n",dev_tmp->err);
		#if 1
		if((dev_tmp->Kp*dev_tmp->err)<HEAT_ALL_CYCLE)
		{
			if(dev_tmp->err>0)
			{
				if((dev_tmp->Ki*dev_tmp->integral)<15)
				{
					dev_tmp->integral+=dev_tmp->err;
				}
			}
			else
			{
				dev_tmp->integral+=dev_tmp->err;
			}
		}
	  //dev_tmp->integral+=dev_tmp->err; old
		
		printf("integral is %f\r\n",dev_tmp->integral);
		//输出的值
	 	dev_tmp->voltage=dev_tmp->Kp*dev_tmp->err+dev_tmp->Ki*dev_tmp->integral+dev_tmp->Kd*(dev_tmp->err-dev_tmp->err_last);

	  dev_tmp->err_last=dev_tmp->err;
		
		if(dev_tmp->voltage>HEAT_ALL_CYCLE)
		{
			dev_tmp->voltage=HEAT_ALL_CYCLE;
		}
		else if(dev_tmp->voltage<0)
		{
			dev_tmp->voltage=0;
		}

		adc_tmp=dev_tmp->voltage;
		#else //add way
		pid_add_tmp=dev_tmp->Kp*(dev_tmp->err-dev_tmp->err_last)+dev_tmp->Ki*dev_tmp->err;
		printf("Hadc_tmp %f\r\n",pid_add_tmp);
		dev_tmp->voltage+=pid_add_tmp;
		printf("voltage %f\r\n",dev_tmp->voltage);
		dev_tmp->err_last=dev_tmp->err;
		if(dev_tmp->voltage>HEAT_ALL_CYCLE)
		{
			adc_tmp=HEAT_ALL_CYCLE;
		}
		else if(dev_tmp->voltage<0)
		{
			adc_tmp=0;
		}
		else
		{
			adc_tmp=dev_tmp->voltage;
		}
		#endif
		printf("Hadc_tmp %d\r\n",adc_tmp);
	  heat_set_status(HEAT_OPEN,0xff,adc_tmp,HEAT_ALL_CYCLE-adc_tmp);
    }
}

void dev_status_check(void)
{
	user_dev_status_t * dev_tmp=dooya_get_dev_info();
	//int16 tmp;
	uint8 tmp_1;
	uint8 tmp_2;
	uint8 tmp_3;
	if((dev_tmp->Now_Status==HEATING)||((dev_tmp->Now_Status==HEATED)))
	{
		//lcd
		if(dev_tmp->Show_Temperature>dev_tmp->Target_Temperature)
		{
			dev_tmp->Show_Temperature--;
		}
		else if(dev_tmp->Show_Temperature<dev_tmp->Target_Temperature)
		{
				dev_tmp->Show_Temperature++;
		}
		else if(dev_tmp->Show_Temperature==dev_tmp->Target_Temperature)
		{
			if(dev_tmp->Now_Status==HEATING)
			{
				led_red_on();
				dev_tmp->Now_Status=HEATED;
			}
			
		}
		tmp_1=dev_tmp->Show_Temperature/100;
		tmp_2=dev_tmp->Show_Temperature%100/10;
		tmp_3=dev_tmp->Show_Temperature%10;
		LCD_load(0,tmp_1);//
		LCD_load(1,tmp_2);//
		LCD_load(2,tmp_3);//
		/**************************************************************/
		if(dev_tmp->Temperature_change_flag)
		{
			
		}
		
	}

	
	if(dev_tmp->Now_Status==HEATED)
	{
		dev_tmp->sleep_number_count++;
	}
	else
	{
		dev_tmp->sleep_number_count=0;
	}
	
	if(dev_tmp->sleep_number_count>100)
	{
		dev_tmp->Now_Status=SLEEP;
		//lcd
	}
	
	
	//tiaowen
	if(dev_tmp->Temperature_change_flag)
	{
		dev_tmp->change_number_count++;
	}
	else
	{	
		dev_tmp->change_number_count=0;
	}

	if(dev_tmp->change_number_count>100)
	{
		dev_tmp->Temperature_change_flag=0;
		//lcd
	}
}

void dev_run_100us (void)
{
	//printf("dev_run_100us\r\n");
	motor_process();
	heat_process();
}

void dev_run_1ms (void)
{
	//printf("dev_run_1ms\r\n");
	LCD_scan();
}

void dev_run_50ms (void)
{
	//printf("dev_run_50ms\r\n");
	adc_value_record();
	PID_realize();
}