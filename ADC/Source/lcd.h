#ifndef __LCD_H_
#define __LCD_H_


#include	"config.h"
#include	"STC8G_H_GPIO.h"

void	LCD_load(u8 n,u8 dat);
void	LCD_scan(void);	
void	Init_LCD_Buffer(void);
void	LCD_load(u8 n,u8 dat);
void	LCD_test(void);




#define DIS_BLACK	0x10
#define DIS_		0x11
#define DIS_A		0x0A
#define DIS_B		0x0B
#define DIS_C		0x0C
#define DIS_D		0x0D
#define DIS_E		0x0E
#define DIS_F		0x0F

#define LCD_SET_DP2		LCD_buff[0] |=  0x08
#define LCD_CLR_DP2		LCD_buff[0] &= ~0x08
#define LCD_FLASH_DP2	LCD_buff[0] ^=  0x08

#define LCD_SET_DP4		LCD_buff[4] |=  0x80
#define LCD_CLR_DP4		LCD_buff[4] &= ~0x80
#define LCD_FLASH_DP4	LCD_buff[4] ^=  0x80

#define LCD_SET_2M		LCD_buff[0] |=  0x20
#define LCD_CLR_2M		LCD_buff[0] &= ~0x20
#define LCD_FLASH_2M	LCD_buff[0] ^=  0x20

#define LCD_SET_4M		LCD_buff[0] |=  0x02
#define LCD_CLR_4M		LCD_buff[0] &= ~0x02
#define LCD_FLASH_4M	LCD_buff[0] ^=  0x02

#define LCD_SET_DP5		LCD_buff[4] |=  0x20
#define LCD_CLR_DP5		LCD_buff[4] &= ~0x20
#define LCD_FLASH_DP5	LCD_buff[4] ^=  0x20

#endif
