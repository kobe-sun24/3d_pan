#include    "heat.h"	
#include	"STC8G_H_GPIO.h"
#include    "string.h"	

typedef struct str_heat_info_t
{
	uint16 times;
	uint16 time_inv_on;
	uint16 time_inv_off;
	uint16 time_inv_on_tmp;
	uint16 time_inv_off_tmp;
	uint8  forever;
	uint8  status;
}str_heat_info;

str_heat_info heat_operate;

void	heat_init(void)
{	
	GPIO_InitTypeDef led_io;
	led_io.Mode=GPIO_OUT_PP;
	led_io.Pin=GPIO_Pin_4;
	GPIO_Inilize(GPIO_P5, &led_io);
	heat_close();
	heat_set_status(HEAT_STOP,0,0,0);
}


void	heat_open(void)	
{
	P54=1;	
}

void	heat_close(void)	
{
	P54=0;
}

void heat_set_status(uint8 status,uint16 times,uint16 time_inv_on,uint16 time_inv_off)
{
	if((times==0xff)&&(heat_operate.forever==1))
	{
		//if((heat_operate.time_inv_on==time_inv_on)&&(heat_operate.time_inv_off==time_inv_off))
		{
			heat_operate.time_inv_on=time_inv_on;
			heat_operate.time_inv_off=time_inv_off;
			heat_operate.status=status;
			return;
		}
	}
	memset(&heat_operate,0x0,sizeof(heat_operate));
	heat_operate.times=times;
	heat_operate.time_inv_on=time_inv_on;
	heat_operate.time_inv_on_tmp=time_inv_on;
	heat_operate.time_inv_off=time_inv_off;
	heat_operate.time_inv_off_tmp=time_inv_off;
	heat_operate.status=status;
	if(times==0xff)
	{
		heat_operate.forever=1;
	}
}


void heat_process(void)
{

	if(heat_operate.times>0)//ms
	{
		if(heat_operate.time_inv_on_tmp>0)
		{
			switch(heat_operate.status)
			{
				case HEAT_OPEN:
					heat_open();
				break;
				case HEAT_STOP:
					heat_close();
				break;

			}
			heat_operate.time_inv_on_tmp--;
		}
		else if(heat_operate.time_inv_off_tmp>0)
		{
			switch(heat_operate.status)
			{
				case HEAT_OPEN:
					heat_close();
				break;
				case HEAT_STOP:
					heat_close();
				break;
			}
			heat_operate.time_inv_off_tmp--;
		}
		if((heat_operate.time_inv_on_tmp==0)&&(heat_operate.time_inv_off_tmp==0))
		{
			if(heat_operate.forever!=1)
			{
				heat_operate.times--;
			}
			heat_operate.time_inv_on_tmp=heat_operate.time_inv_on;
			heat_operate.time_inv_off_tmp=heat_operate.time_inv_off;
		}
	}
	else
	{
		heat_operate.status=HEAT_STOP;
		heat_close();
	}

}


