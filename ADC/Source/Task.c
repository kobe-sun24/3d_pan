#include	"Task.h"
#include    "lcd.h"
#include    "motor.h"	
#include    "nRF24l01p.h"
#include    "heat.h"
#include	"device_status.h"
#include    "adc.h"	
//========================================================================
//                               本地变量声明
//========================================================================
//100us once
static TASK_COMPONENTS Task_Comps[]=
{
//状态  计数  周期  函数
	{0, 50, 50, dev_run_50ms},  	     /* task 3 Period： 50ms */
	{0, 1000, 1000, NRF24L01_recive_process},  	 /* task 3 Period： 100ms */
	{0, 5000, 5000, dev_status_check},           /* task 6 Period： 500ms */
//	{0, 1, 1, Sample_EEPROM},           /* task 7 Period： 1ms */
//	{0, 100, 100, Sample_WDT},          /* task 8 Period： 100ms */
//	{0, 1, 1, Sample_PWMA_Output},      /* task 9 Period： 1ms */
//	{0, 1, 1, Sample_PWMB_Output},      /* task 9 Period： 1ms */
//	{0, 10, 10, Sample_PCA_PWM},        /* task 10 Period：10ms */
//	{0, 1, 1, Sample_PCA_Capture},      /* task 11 Period：1ms */
//	{0, 1, 1, Sample_PWM15bit},         /* task 12 Period：1ms */
	/* Add new task here */
};

u8 Tasks_Max = sizeof(Task_Comps)/sizeof(Task_Comps[0]);

//========================================================================
// 函数: Task_Handler_Callback
// 描述: 任务标记回调函数.
// 参数: None.
// 返回: None.
// 版本: V1.0, 2012-10-22
//========================================================================
void Task_Marks_Handler_Callback(void)
{
	u8 i;
	for(i=0; i<Tasks_Max; i++)
	{
		if(Task_Comps[i].TIMCount)    /* If the time is not 0 */
		{
			Task_Comps[i].TIMCount--;  /* Time counter decrement */
			if(Task_Comps[i].TIMCount == 0)  /* If time arrives */
			{
				/*Resume the timer value and try again */
				Task_Comps[i].TIMCount = Task_Comps[i].TRITime;  
				Task_Comps[i].Run = 1;    /* The task can be run */
			}
		}
	}
}

//========================================================================
// 函数: Task_Pro_Handler_Callback
// 描述: 任务处理回调函数.
// 参数: None.
// 返回: None.
// 版本: V1.0, 2012-10-22
//========================================================================
void Task_Pro_Handler_Callback(void)
{
	u8 i;
	for(i=0; i<Tasks_Max; i++)
	{
		if(Task_Comps[i].Run) /* If task can be run */
		{
			Task_Comps[i].Run = 0;    /* Flag clear 0 */
			if(Task_Comps[i].TaskHook)
			{
				Task_Comps[i].TaskHook();  /* Run task */
			}
		}
	}
}


