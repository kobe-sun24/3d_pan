#ifndef __LED_H_
#define __LED_H_


#include	"config.h"
#include	"STC8G_H_GPIO.h"
void	led_config(void);
void	led_red_on(void)	;
void	led_green_on(void)	;
void	led_all_close(void)	;
#endif
