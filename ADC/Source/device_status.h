#ifndef __DEVICE_STATUS_H_
#define __DEVICE_STATUS_H_

#include    <stdio.h>
#include	"config.h"

#define POWER_INIT (1)
#define HEATING    (2)
#define HEATED     (3)
#define SLEEP      (4)

#define SPEED_MAX  (9)
#define ADC_MAX    (3)

#define MOTOR_ALL_CYCLE (30)//3MS
#define HEAT_ALL_CYCLE  (50)//5MS

typedef struct _user_dev_status_t{

	uint8  Now_Status;
	uint8  Now_Speed;
	uint8  Now_Motor_Status;	
	uint16 Now_Adc_index;
	uint16 Now_Adc_val[ADC_MAX];
	int16  Show_Temperature;	
	/************************************************************/
	int16  Now_Temperature;//定义实际值
	int16  Target_Temperature;//定义设定值
	float  err;                //定义偏差值
	float  err_last;           //定义上一个偏差值
	float  Kp,Ki,Kd;            //定义比例、积分、微分系数
	float  voltage;             //定义电压值（控制执行器的变量）
	float  integral;            //定义积分值
	/************************************************************/
	uint16 change_number_count;
	uint8  Temperature_change_flag;
	uint16 sleep_number_count;
}user_dev_status_t;

void dev_status_init(void);
	
uint8 get_dev_Status(void  );
int16 get_dev_Temperature(void);
uint8 get_dev_Speed(void);
uint8 get_dev_Motor_Status(void  );
uint16 get_dev_ADC_val(uint16       data_index);
uint16 get_dev_adc_index(void  );
int16 get_dev_ShowTemperature(void  );
int16 get_dev_NewTemperature(void  );

void set_dev_Status(uint8 data_tmp);
void set_dev_Temperature(int16 data_tmp);
void set_dev_Speed(uint8 data_tmp);
void set_dev_Motor_Status(uint8 data_tmp);
void set_dev_ADC_val(uint16       data_index,uint16 data_tmp);
void set_dev_ADC_index(uint16       data_index);
void set_dev_ShowTemperature(int16 data_tmp  );
/***********************fun**************************************/
void motor_run_leavel(uint8 leavel);
void PID_realize(void);
void dev_status_check(void);
void dev_run_100us (void);
void dev_run_1ms (void);
void dev_run_50ms (void);
#endif
