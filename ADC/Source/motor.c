#include    "motor.h"	
#include	"STC8G_H_GPIO.h"
#include    "string.h"





str_motor_info motor_operate;


void	motor_init(void)
{	
	GPIO_InitTypeDef led_io;
	led_io.Mode=GPIO_PullUp;
	led_io.Pin=GPIO_Pin_2|GPIO_Pin_3;
	GPIO_Inilize(GPIO_P3, &led_io);
	P32=0;
	P33=0;
	motor_set_status(MOTOR_STOP,0,0,0);
}


void	motor_forward(void)	
{
	P32=1;
	P33=0;
}

void	motor_reverse(void)	
{
	P32=0;
	P33=1;
}

void	motor_Brake(void)	
{
	P32=1;
	P33=1;

}

void	motor_stop(void)	
{
	P32=0;
	P33=0;

}

void motor_set_status(uint8 status,uint16 times,uint16 time_inv_on,uint16 time_inv_off)
{
	if((times==0xff)&&(motor_operate.forever==1))
	{
		//if((motor_operate.time_inv_on==time_inv_on)&&(motor_operate.time_inv_off==time_inv_off))
		{
			motor_operate.time_inv_on=time_inv_on;
			motor_operate.time_inv_off=time_inv_off;
			motor_operate.status=status;
			return;
		}
	}
	memset(&motor_operate,0x0,sizeof(motor_operate));
	motor_operate.times=times;
	motor_operate.time_inv_on=time_inv_on;
	motor_operate.time_inv_on_tmp=time_inv_on;
	motor_operate.time_inv_off=time_inv_off;
	motor_operate.time_inv_off_tmp=time_inv_off;
	motor_operate.status=status;
	if(times==0xff)
	{
		motor_operate.forever=1;
	}
	else
	{
		motor_operate.forever=0;
	}
}


void motor_process(void)
{

	if(motor_operate.times>0)//ms
	{
		if(motor_operate.time_inv_on_tmp>0)
		{
			switch(motor_operate.status)
			{
				case MOTOR_FORWARD:
					motor_forward();
				break;
				case MOTOR_REVERSE:
					motor_reverse();
				break;
				case MOTOR_BRAKE:
					motor_Brake();
				break;
				case MOTOR_STOP:
					motor_stop();
				break;
			}
			motor_operate.time_inv_on_tmp--;
		}
		else if(motor_operate.time_inv_off_tmp>0)
		{
			switch(motor_operate.status)
			{
				case MOTOR_FORWARD:
					motor_stop();
				break;
				case MOTOR_REVERSE:
					motor_stop();
				break;
				case MOTOR_BRAKE:
					motor_stop();
				break;
				case MOTOR_STOP:
					motor_stop();
				break;
			}
			motor_operate.time_inv_off_tmp--;
		}
		if((motor_operate.time_inv_on_tmp==0)&&(motor_operate.time_inv_off_tmp==0))
		{
			if(motor_operate.forever!=1)
			{
				motor_operate.times--;
			}
			motor_operate.time_inv_on_tmp=motor_operate.time_inv_on;
			motor_operate.time_inv_off_tmp=motor_operate.time_inv_off;
		}
	}
	else
	{
		motor_operate.status=MOTOR_STOP;
		motor_stop();
	}

}





