#include "led.h"	
#include	"STC8G_H_Delay.h"
 /**************************************************************************
功能描述：LED口初始化
入口参数：无
返回值：无
 *************************************************************************/
void	led_config(void)
{	

}


void	led_red_on(void)	
{
	GPIO_InitTypeDef led_io;
	led_io.Mode=GPIO_PullUp;
	led_io.Pin=GPIO_Pin_7;
	GPIO_Inilize(GPIO_P2, &led_io);
	P27=1;
}

void	led_green_on(void)	
{
	GPIO_InitTypeDef led_io;
	led_io.Mode=GPIO_PullUp;
	led_io.Pin=GPIO_Pin_7;
	GPIO_Inilize(GPIO_P2, &led_io);
	P27=0;
}

void	led_all_close(void)	
{
	GPIO_InitTypeDef led_io;
	led_io.Mode=GPIO_HighZ;
	led_io.Pin=GPIO_Pin_7;
	GPIO_Inilize(GPIO_P2, &led_io);
}
