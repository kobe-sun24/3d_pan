#ifndef __HEAT_H_
#define __HEAT_H_

#include	"config.h"

#define HEAT_OPEN (1)
#define HEAT_STOP (2)

void	heat_init(void);
void	heat_open(void)	;
void	heat_close(void);	
void heat_set_status(uint8 status,uint16 times,uint16 time_inv_on,uint16 time_inv_off);
void heat_process(void);


#endif
