#ifndef __MOTOR_H_
#define __MOTOR_H_

#include	"config.h"

#define MOTOR_FORWARD (1)
#define MOTOR_REVERSE (2)
#define MOTOR_BRAKE   (3)
#define MOTOR_STOP    (4)

typedef struct str_motor_info_t
{
	uint16 times;
	uint16 time_inv_on;
	uint16 time_inv_off;
	uint16 time_inv_on_tmp;
	uint16 time_inv_off_tmp;
	uint8  forever;
	uint8  status;
}str_motor_info;

extern str_motor_info motor_operate;

void	motor_init(void);
void	motor_forward(void);
void	motor_reverse(void);
void	motor_Brake(void);
void	motor_stop(void);
void motor_set_status(uint8 status,uint16 times,uint16 time_inv_on,uint16 time_inv_off);
void motor_process(void);


#endif
