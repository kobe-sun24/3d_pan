#include "nRF24l01p.h"
#include "led.h"
#include "device_status.h"
#include "motor.h"	

uint8  RxPayload[32];   //无线接收缓存
uint8  TxPayload[32];   //无线发送缓存

const uint8 TX_ADDRESS[TX_ADDR_WIDTH]={0xE1,0xE2,0xE3,0xE4,0xE5}; //发送地址
const uint8 RX_ADDRESS[RX_ADDR_WIDTH]={0xE1,0xE2,0xE3,0xE4,0xE5}; //接收地址		

/***************************************************************************
 * 描  述 : 模拟SPI读写数据函数，读写一个字节
 * 入  参 : 写入的数据
 * 返回值 : 读取的数据
 **************************************************************************/
uint8 SPI_RW(uint8 byte)
{
  uint8 bit_ctr;

  for(bit_ctr = 0; bit_ctr < 8; bit_ctr++)
	{
		 if((byte&0x80)==0x80)
     { RF_SPI_MOSI=1;  }
		 else 
     { RF_SPI_MOSI=0;  }
		 
		 byte =byte<< 1;
	   RF_SPI_SCK=1;
		 
		if(RF_SPI_MISO) 
    {byte++; }
     RF_SPI_SCK=0 ;
  }
   return(byte);
}

/***************************************************************************
 * 描  述 : NRF24L01寄存器写函数
 * 入  参 : regaddr：要写的寄存器地址；data：写入到寄存器的数据
 * 返回值 : 读取的状态值
 **************************************************************************/
uint8 NRF24L01_MA_Write_Reg(uint8 regaddr,uint8 dat)
{
	uint8 status;	
	
  CS_MA_LOW;                             //使能SPI传输
	status =SPI_RW(regaddr);               //发送寄存器地址
  SPI_RW(dat);                           //写入寄存器的值
  CS_MA_HIGH;                            //禁止SPI传输	
  return(status);       		             //返回状态值
}

/***************************************************************************
 * 描  述 : NRF24L01寄存器读函数
 * 入  参 : regaddr:要读取的寄存器地址
 * 返回值 : 读取的寄存器的值
 **************************************************************************/
uint8 NRF24L01_MA_Read_Reg(uint8 regaddr)
{
	uint8 reg_val;	  
  
 	CS_MA_LOW;                          //使能SPI传输	
	SPI_RW(regaddr);                    //发送寄存器号
	reg_val=SPI_RW(0XFF);               //读取寄存器内容
  CS_MA_HIGH;                         //禁止SPI传输		
  return(reg_val);                    //返回读取的值
}	

/***************************************************************************
 * 描  述 : 从指定地址读出指定长度的数据
 * 入  参 : pBuf:读出数据的存放地址；datlen：读出的数据字节数
 * 返回值 : 读取的寄存器的值
 **************************************************************************/
uint8 NRF24L01_MA_ReadBuf(uint8 regaddr,uint8 *pBuf,uint8 datlen)
{
	uint8 status,temp;	       
  CS_MA_LOW;                               //使能SPI
  status=SPI_RW(regaddr);                  //发送寄存器值,并读取状态值   	   
 	for(temp=0;temp<datlen;temp++)
	{
		pBuf[temp]=SPI_RW(0XFF);               //读出数据
	}
  CS_MA_HIGH;                              //禁止SPI
  return status;                           //返回读到的状态值
}

/***************************************************************************
 * 描  述 : 向指定地址写入指定长度的数据
 * 入  参 : pBuf:读出数据的存放地址；datlen：读出的数据字节数
 * 返回值 : 读取的状态寄存器值
 **************************************************************************/
uint8 NRF24L01_MA_Write_Buf(uint8 regaddr, uint8 *pBuf, uint8 datalen)
{
	uint8 status,temp;	    
 	CS_MA_LOW;                                              //使能SPI传输
	status = SPI_RW(regaddr);                               //发送寄存器值,并读取状态值 
  for(temp=0; temp<datalen; temp++)
  {
	  SPI_RW(*pBuf++);                                      //写入数据		
  }
  CS_MA_HIGH;                                             //关闭SPI传输
  return status;                                          //返回读到的状态值
}	

/***************************************************************************
 * 描  述 : 启动NRF24L01发送一次数据
 * 入  参 : buf:待发送数据首地址
 * 返回值 : 发送完成状况
 **************************************************************************/
uint8 NRF24L01_MA_TxPacket(uint8 *buf)
{
	uint8 state;   
	
	CE_MA_LOW;
  NRF24L01_MA_Write_Buf(WR_TX_PLOAD,buf,TX_PLOAD_WIDTH);  //写数据到TX BUF 
 	CE_MA_HIGH;                                             //启动发送	   
	while(RF_IRQ!=0);                                          //等待发送完成
	state=NRF24L01_MA_Read_Reg(STATUS);                     //读取状态寄存器的值	   
	NRF24L01_MA_Write_Reg(SPI_WRITE_REG+STATUS,state);      //清除RX_DR、TX_DS或MAX_RT中断标志
	if(state&MAX_TX)                                        //达到最大重发次数
	{
		NRF24L01_MA_Write_Reg(FLUSH_TX,0xff);                 //清除TX FIFO寄存器 
		return MAX_TX;                                        //返回0x10
	}
	if(state&TX_OK)                                         //发送完成
	{
		return TX_OK;                                         //返回0x20
	}
	return 0xff;                                            //发送失败，返回0xff
}

/***************************************************************************
 * 描  述 : 启动NRF24L01读取一次数据
 * 入  参 : buf:待发送数据首地址
 * 返回值 : 0：未接收到数据；RX_OK：接收到数据
 **************************************************************************/
uint8 NRF24L01_MA_RxPacket(uint8 *buf)
{
	uint8 state;		    
	
	state=NRF24L01_MA_Read_Reg(STATUS);                     //读取状态寄存器的值    	 
	NRF24L01_MA_Write_Reg(SPI_WRITE_REG+STATUS,state);      //清除RX_DR、TX_DS或MAX_RT中断标志
	if(state&RX_OK)                                         //接收到数据
	{
		NRF24L01_MA_ReadBuf(RD_RX_PLOAD,buf,RX_PLOAD_WIDTH);  //读取数据
		NRF24L01_MA_Write_Reg(FLUSH_RX,0xff);                 //清除RX FIFO寄存器 
		return RX_OK;                                         //返回0x10
	}	   
	return 0;                                               //没收到任何数据
}

/***************************************************************************
 * 描  述 : 初始化NRF24L01到RX模式，配置相关参数，CE变高后,即进入RX模式
 * 入  参 : 无
 * 返回值 : 无
 **************************************************************************/
void Set_RxMode_MA(void)
{
	CE_MA_LOW;	  
  //写RX节点地址
  NRF24L01_MA_Write_Buf(SPI_WRITE_REG+RX_ADDR_P0,(uint8 *)RX_ADDRESS,RX_ADDR_WIDTH);
  //使能通道0的自动应答    
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+EN_AA,0x01);    
  //使能通道0的接收地址  	 
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+EN_RXADDR,0x01);
  //设置RF通道工作频率		  
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+RF_CH,55);	     
  //选择通道0的有效数据宽度 	    
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+RX_PW_P0,RX_PLOAD_WIDTH);
  //设置TX发射参数,0db增益,250Kbps,低噪声增益开启  
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+RF_SETUP,0x26);
  //设置为接收模式PRX,使能PWR_UP、EN_CRC,使能CRC为2字节，开启RX_DR、TX_DS、MAX_RT中断引脚 
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+CONFIG, 0x0f); 
  //CE为高,进入接收模式 
  CE_MA_HIGH;  	
}	

/***************************************************************************
 * 描  述 : 初始化NRF24L01到TX模式，配置相关参数
 * 入  参 : 无
 * 返回值 : 无
 **************************************************************************/
void Set_TxMode_MA(void)
{														 
	CE_MA_LOW;	    
  //写TX节点地址 
  NRF24L01_MA_Write_Buf(SPI_WRITE_REG+TX_ADDR,(uint8*)TX_ADDRESS,TX_ADDR_WIDTH);    
  //设置RX节点地址,使能ACK（应答）		  
  NRF24L01_MA_Write_Buf(SPI_WRITE_REG+RX_ADDR_P0,(uint8*)RX_ADDRESS,RX_ADDR_WIDTH); 
  //使能通道0的自动应答    
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+EN_AA,0x01);     
  //使能通道0的接收地址  
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+EN_RXADDR,0x01); 
  //设置自动重发间隔时间:750us+86us ;最大自动重发次数:5次
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+SETUP_RETR,0x25);
  //设置RF通道工作频率	
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+RF_CH,55);       
  //设置TX发射参数,0db增益,250Kbps,低噪声增益开启   
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+RF_SETUP,0x26);  
  //设置为发送模式PTX,使能PWR_UP、EN_CRC,使能CRC为2字节，开启RX_DR、TX_DS、MAX_RT中断引脚 
  NRF24L01_MA_Write_Reg(SPI_WRITE_REG+CONFIG,0x0e);                                  
}			  

/***************************************************************************
 * 描  述 : NRF24L01初始化函数，初始化连接NRF24L01模块的管脚，调用SPI初始化函数
 *          完成和NRF24L01模块通讯的SPI总线的初始化
 * 入  参 : 无
 * 返回值 : 无
 **************************************************************************/
void Init_NRF24L01_MA(void)
{	
	delay_ms(2);
	//led_on(LED_3);                               //初始化点亮用户指示灯D3
	CE_MA_LOW; 	                                 //使能NRF24L01
	CS_MA_HIGH;                                  //SPI片选取消
	RF_SPI_SCK=0 ;                               //SPI时钟初始置低电平
}

/*********************************END FILE************************************/

//#define RF_EN 		P20         //使能控制
//#define RF_SPI_CS   P21         //模拟SPI片选 
//#define RF_SPI_SCK  P25         //模拟SPI时钟
//#define RF_SPI_MOSI P23         //模拟SPI主出从入 
//#define RF_SPI_MISO P24         //模拟SPI主入从出 
//#define RF_IRQ      P22         //中断 

void Init_NRF24L01 (void)
{

	GPIO_InitTypeDef led_io;
	led_io.Mode=GPIO_PullUp;
	led_io.Pin=GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5;
	GPIO_Inilize(GPIO_P2, &led_io);

	Init_NRF24L01_MA();	          //初始化nRF24L01
	Set_RxMode_MA();	  					//配置nRF24L01为发送模式
}


void NRF24L01_recive_process (void)
{
	u8 tmp;
	if(NRF24L01_MA_RxPacket(RxPayload) == RX_OK)
	{
		printf("RxPayload[0] %x\r\n",RxPayload[0]); 
		switch(get_dev_Status())
		{
			case POWER_INIT:
				//printf("POWER_INIT\r\n");
				if(RxPayload[0] == 0x52)//add
				{
					set_dev_Status(HEATING);
					led_green_on();
					//set_dev_ShowTemperature(get_dev_NewTemperature());
					set_dev_ShowTemperature(200);//tmp
				}
			break;
			case HEATING:
			break;
			case HEATED:
				tmp=get_dev_Speed();
				if(RxPayload[0] == 0x50)//add
				{
					if(tmp<SPEED_MAX)
					{
						tmp++;
						printf("tmp %d\r\n",tmp);
						set_dev_Speed(tmp);
					}
					
				}
				else if(RxPayload[0] == 0x51)
				{
					if(tmp>0)
					{
						tmp--;
						printf("tmp %d\r\n",tmp); 
						set_dev_Speed(tmp);
					}
				}
				else if(RxPayload[0] == 0x52)//run
				{
					if(get_dev_Motor_Status())
					{
						set_dev_Motor_Status(0);
						motor_set_status(MOTOR_REVERSE,10,5000,0);
					}
					else
					{
						motor_run_leavel(get_dev_Speed());
						set_dev_Motor_Status(1);
					}

				}
				else if(RxPayload[0] == 0x53)//退
				{
					if(get_dev_Motor_Status())
					{
						//dooya_set_dev_Motor_Status(0);
						//motor_set_status(MOTOR_REVERSE,1,50000,0);
					}
					else
					{
						//motor_run_leavel(dooya_get_dev_Speed());
						//dooya_set_dev_Motor_Status(1);
						motor_set_status(MOTOR_REVERSE,1500,100,0);
					}

				}
				RxPayload[0]=0;
			break;
			case SLEEP:
			break;
		}
	}
}


